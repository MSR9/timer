//
//  ViewController.swift
//  Timer
//
//  Created by EPITADMBP04 on 4/7/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var playpauseButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    
    var isTimerOn = false
    
    var duration = 0
    var timer = Timer()
    
    @IBAction func didTapStartOrPause(_ sender: UIButton) {
        isTimerOn.toggle()
        showPlayButton(!isTimerOn)
        toggleTimer(on: isTimerOn)
        self.resetButton.backgroundColor = .red
    }
    
    func toggleTimer(on: Bool) {
        
        if on {
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] (_) in
            guard let strongSelf = self else { return }
            strongSelf.duration += 1
            strongSelf.label.text = String(strongSelf.duration)
        })
        } else {
            timer.invalidate()
        }
    }
    
    func showPlayButton(_ shouldShowPlayButton: Bool) {
        let imageName = shouldShowPlayButton ? "playIcon" : "pauseIcon"
        playpauseButton.setImage(UIImage(named: imageName), for: .normal)
        playpauseButton.backgroundColor = shouldShowPlayButton ? .green : .yellow
    }
    
    @IBAction func didTapReset() {
        
        timer.invalidate()
        duration = 0
        
        label.text = String(duration)
        showPlayButton(true)
    }
}

